---
title: "About"
permalink: /about/
header:
    image: "/images/3.JPG"
---
Hi. I'am Srinivas.

PERSONAL DETAILS
<br>

A few interesting things about me. I love to read science fiction. I am also an avid gamer. Lastly, I love learning. Every day I push myself to learn something new, whether that be about machine learning, software engineering, or miscellaneous facts about the universe.
<br>

I just finished my diploma program in Artificial Intelligence at National Institute of Electronics and Information Technology where I have learnt many concepts in machine learning, deep learning and reinforcement learning. I plan on searching for Data Science positions in Hyderabad, Mumbai, and Bangalore.

To my blog: [ Medium ](https://medium.com/@sapireddyrahul)

Liked this website and want to here more?
<form action="https://formspree.io/sapireddyrahul@gmail.com"
      method="POST">
    <input type="text" placeholder="Name" name="name">
    <input type="text" placeholder="Question" name="Question">
    <input type="email" placeholder="Email" name="_replyto" required><input type="submit" value="Send">
</form>

<a href="https://www.testdome.com/cert/cf44c90a71584acea3434a0a53663e5c" class="testdome-certificate-stamp gold"><span class="testdome-certificate-name">Sapireddy Sriniva...</span><span class="testdome-certificate-test-name">Python </span><span class="testdome-certificate-card-logo">TestDome<br />Certificate</span></a><script>var stylesheet = "https://www.testdome.com/content/source/stylesheets/embed.css", link = document.createElement("link"); link.href = stylesheet, link.type = "text/css", link.rel = "stylesheet", link.media = "screen,print", document.getElementsByTagName("head")[0].appendChild(link);</script>
